# Rock–paper–scissors
 [![pipeline status](https://gitlab.com/jannik-hh/rock_paper_scissors/badges/master/pipeline.svg)](https://gitlab.com/jannik-hh/rock_paper_scissors/commits/master)
[![coverage report](https://gitlab.com/jannik-hh/rock_paper_scissors/badges/master/coverage.svg)](https://gitlab.com/jannik-hh/rock_paper_scissors/commits/master)

This spring application provides a simple JSON API to play the Rock-paper-scissors game.

# Usage
## Build and run project

### With docker-compose

Simply run `docker-compose up`

### On your machine
Make sure you have a machine equipped with maven und the Java JDK 8.

1. Build the project with
`mvn install`

2. Run the project with
`java -jar target/rock_paper_scissors-0.0.1-SNAPSHOT.jar`


## Play the game

Play one round of the game:

```sh
curl -X POST \
  http://localhost:8080/games \
  -H 'Content-Type: application/json' \
  -d '{
	"playerMove": "Rock"
}'
```

As moves "Rock","Scissors" and "Paper" are available.
