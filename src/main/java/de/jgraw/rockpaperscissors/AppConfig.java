package de.jgraw.rockpaperscissors;

import de.jgraw.rockpaperscissors.services.MoveChooser;
import de.jgraw.rockpaperscissors.services.RandomMoveChooser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

  @Bean
  public MoveChooser moveChooser() {
    return new RandomMoveChooser();
  }
}
