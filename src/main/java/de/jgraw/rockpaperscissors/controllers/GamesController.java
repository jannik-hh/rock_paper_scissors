package de.jgraw.rockpaperscissors.controllers;

import de.jgraw.rockpaperscissors.models.Game;
import de.jgraw.rockpaperscissors.models.GameRequest;
import de.jgraw.rockpaperscissors.services.MoveChooser;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GamesController {

  @Autowired private MoveChooser moveChooser;

  @RequestMapping(
    path = "/games",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public Game create(@Valid @RequestBody GameRequest gameRequest) {
    return new Game(gameRequest.getPlayerMove(), moveChooser.getMove());
  }
}
