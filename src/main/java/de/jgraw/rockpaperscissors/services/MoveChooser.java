package de.jgraw.rockpaperscissors.services;

import de.jgraw.rockpaperscissors.models.Move;

public interface MoveChooser {
  Move getMove();
}
