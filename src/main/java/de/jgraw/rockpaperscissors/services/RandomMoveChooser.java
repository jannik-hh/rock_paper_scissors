package de.jgraw.rockpaperscissors.services;

import de.jgraw.rockpaperscissors.models.Move;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomMoveChooser implements MoveChooser {
  private final int upperIndexBound = Move.values().length;
  private final List<Move> moves = Arrays.asList(Move.values());
  private final Random random;

  public RandomMoveChooser() {
    this.random = new Random();
  }

  public RandomMoveChooser(Random random) {
    this.random = random;
  }

  /**
   * Chooses a random value from the Move enum and returns it.
   *
   * @return a random Move value.
   */
  @Override
  public Move getMove() {
    int randomIndex = random.nextInt(upperIndexBound);
    return moves.get(randomIndex);
  }
}
