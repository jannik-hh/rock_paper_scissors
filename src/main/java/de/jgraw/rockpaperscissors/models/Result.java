package de.jgraw.rockpaperscissors.models;

public enum Result {
  WIN,
  DRAW,
  LOOSE
}
