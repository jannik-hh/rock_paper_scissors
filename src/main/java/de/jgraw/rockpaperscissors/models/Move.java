package de.jgraw.rockpaperscissors.models;

public enum Move {
  Rock {
    @Override
    boolean winsAgainst(Move move) {
      return Scissors == move;
    }
  },

  Paper {
    @Override
    boolean winsAgainst(Move move) {
      return move == Rock;
    }
  },

  Scissors {
    @Override
    boolean winsAgainst(Move move) {
      return move == Paper;
    }
  };

  /**
   * Returns the result of the game from the point of the move on which the method is called on.
   *
   * @param move the move of the other player.
   * @return the result of the game.
   */
  public Result gameResultAgainst(Move move) {
    if (this == move) {
      return Result.DRAW;
    } else if (this.winsAgainst(move)) {
      return Result.WIN;
    } else {
      return Result.LOOSE;
    }
  }

  abstract boolean winsAgainst(Move move);
}
