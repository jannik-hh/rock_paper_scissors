package de.jgraw.rockpaperscissors.models;

public class Game {
  private final Move playerMove;
  private final Move computerMove;

  public Game(Move playerMove, Move computerMove) {
    this.playerMove = playerMove;
    this.computerMove = computerMove;
  }

  public Move getPlayerMove() {
    return playerMove;
  }

  public Move getComputerMove() {
    return computerMove;
  }

  public Result getResult() {
    return playerMove.gameResultAgainst(computerMove);
  }
}
