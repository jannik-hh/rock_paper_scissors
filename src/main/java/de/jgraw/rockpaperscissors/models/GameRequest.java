package de.jgraw.rockpaperscissors.models;

import javax.validation.constraints.NotNull;

public class GameRequest {
  @NotNull(message = "playerMove must not be blank!")
  private Move playerMove;

  public GameRequest() {}

  public GameRequest(Move playerMove) {
    this.playerMove = playerMove;
  }

  public Move getPlayerMove() {
    return playerMove;
  }

  public void setPlayerMove(Move playerMove) {
    this.playerMove = playerMove;
  }
}
