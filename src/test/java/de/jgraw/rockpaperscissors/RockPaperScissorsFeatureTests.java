package de.jgraw.rockpaperscissors;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.jgraw.rockpaperscissors.models.Move;
import de.jgraw.rockpaperscissors.services.MoveChooser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RockPaperScissorsFeatureTests {
  @Autowired private MockMvc mockMvc;
  @MockBean private MoveChooser moveChooser;

  @Before
  public void beforeEach() {
    when(moveChooser.getMove()).thenReturn(Move.Paper);
  }

  @Test
  public void playGameChoosingRock() throws Exception {
    mockMvc
        .perform(
            post("/games")
                .content("{\"playerMove\": \"Rock\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.playerMove").value("Rock"))
        .andExpect(jsonPath("$.computerMove").value("Paper"))
        .andExpect(jsonPath("$.result").value("LOOSE"));
  }

  @Test
  public void playGameChoosingScissors() throws Exception {
    mockMvc
        .perform(
            post("/games")
                .content("{\"playerMove\": \"Scissors\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.playerMove").value("Scissors"))
        .andExpect(jsonPath("$.computerMove").value("Paper"))
        .andExpect(jsonPath("$.result").value("WIN"));
  }

  @Test
  public void playGameChoosingPaper() throws Exception {
    mockMvc
        .perform(
            post("/games")
                .content("{\"playerMove\": \"Paper\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.playerMove").value("Paper"))
        .andExpect(jsonPath("$.computerMove").value("Paper"))
        .andExpect(jsonPath("$.result").value("DRAW"));
  }

  @Test
  public void withInvalidPlayerMoveServerShouldReturnBadRequest() throws Exception {
    mockMvc
        .perform(
            post("/games")
                .content("{\"playerMove\": \"Bomb\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void withoutPlayerMoveServerShouldReturnBadRequest() throws Exception {
    mockMvc
        .perform(post("/games").content("{}").contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }
}
