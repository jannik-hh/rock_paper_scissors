package de.jgraw.rockpaperscissors.services;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import de.jgraw.rockpaperscissors.models.Move;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Test;

public class RandomMoveChooserTest {
  @Test
  public void getMoveReturnsRandomMovesUniformlyDistributed() {
    Random random = new Random(1);
    RandomMoveChooser randomMoveChooser = new RandomMoveChooser(random);
    List<Move> moves =
        IntStream.range(0, 1000)
            .mapToObj(i -> randomMoveChooser.getMove())
            .collect(Collectors.toList());

    Map<Move, Double> frequencyMap = calculatePercentageFrequencyDistribution(moves);

    assertEquals(frequencyMap.keySet(), new HashSet(Arrays.asList(Move.values())));

    double expectedPercentagePerMove = 1.0 / Move.values().length;
    assertThat(frequencyMap.values(), everyItem(closeTo(expectedPercentagePerMove, 0.05)));
  }

  private <T> Map<T, Double> calculatePercentageFrequencyDistribution(List<T> list) {
    Double streamSize = Double.valueOf(list.size());
    Function<Long, Double> countToPercentage = (count) -> Double.valueOf(count) / streamSize;
    Map<T, Double> frequencyPercentageMap =
        list.stream()
            .collect(
                groupingBy(
                    Function.identity(),
                    Collectors.collectingAndThen(counting(), countToPercentage)));

    return frequencyPercentageMap;
  }
}
