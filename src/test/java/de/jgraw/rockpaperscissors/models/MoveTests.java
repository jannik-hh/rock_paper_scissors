package de.jgraw.rockpaperscissors.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MoveTests {
  @Test
  public void paperWinsAgainstRock() {
    Result result = Move.Paper.gameResultAgainst(Move.Rock);

    assertEquals(Result.WIN, result);
  }

  @Test
  public void paperLoosesAgainstScissors() {
    Result result = Move.Paper.gameResultAgainst(Move.Scissors);

    assertEquals(Result.LOOSE, result);
  }

  @Test
  public void paperAgainstPaperEndsInADraw() {
    Result result = Move.Paper.gameResultAgainst(Move.Paper);

    assertEquals(Result.DRAW, result);
  }

  @Test
  public void rockWinsAgainstScissors() {
    Result result = Move.Rock.gameResultAgainst(Move.Scissors);

    assertEquals(Result.WIN, result);
  }

  @Test
  public void rockLoosesAgainstPaper() {
    Result result = Move.Rock.gameResultAgainst(Move.Paper);

    assertEquals(Result.LOOSE, result);
  }

  @Test
  public void rockAgainstRockEndsInADraw() {
    Result result = Move.Rock.gameResultAgainst(Move.Rock);

    assertEquals(Result.DRAW, result);
  }

  @Test
  public void scissorsWinsAgainstPaper() {
    Result result = Move.Scissors.gameResultAgainst(Move.Paper);

    assertEquals(Result.WIN, result);
  }

  @Test
  public void scissorsLoosesAgainstRock() {
    Result result = Move.Scissors.gameResultAgainst(Move.Rock);

    assertEquals(Result.LOOSE, result);
  }

  @Test
  public void scissorsAgainstScissorsEndsInADraw() {
    Result result = Move.Scissors.gameResultAgainst(Move.Scissors);

    assertEquals(Result.DRAW, result);
  }
}
