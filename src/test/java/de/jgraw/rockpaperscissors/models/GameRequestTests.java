package de.jgraw.rockpaperscissors.models;

import static org.junit.Assert.assertEquals;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.BeforeClass;
import org.junit.Test;

public class GameRequestTests {
  private static Validator validator;

  @BeforeClass
  public static void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void playerMoveIsNull() {
    GameRequest gameRequest = new GameRequest(null);
    Set<ConstraintViolation<GameRequest>> constraintViolations = validator.validate(gameRequest);
    assertEquals(1, constraintViolations.size());
    assertEquals(
        "playerMove must not be blank!", constraintViolations.iterator().next().getMessage());
  }
}
