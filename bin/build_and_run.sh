#!/bin/bash
set -o errexit
set -o nounset

base_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/../

mvn install -f $base_dir/pom.xml
java -jar $base_dir/target/rock_paper_scissors-0.0.1-SNAPSHOT.jar
